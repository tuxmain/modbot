#!/usr/bin/env python3

"""
	CopyLeft 2019 Pascal Engélibert

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import utils

NAME = "std_clarity"
DESCRIPTION = "Checks for clarity in posts"
AUTHORS = ["tuxmain <tuxmain@zettascript.org>"]
SETTINGS = {
	"post_chars_count_max": 10000,
	"warnings": 3,
	"moderate_staff": False
}

ITEMS = {
	"too_long": """**Message trop long**&nbsp;: cela gêne la lecture du topic et le déroulement d'un débat. Veillez à laisser du temps aux autres pour répondre.""",
	"no_paragraphs": """**Paragraphes trop longs**&nbsp;: un message structuré en petits paragraphes est plus lisible.""",
	"no_punctuation": """**Peu de ponctuation**&nbsp;: utiliser plus de ponctuation rendrait vos phrases plus lisibles."""
}
MESSAGE_TEXT = """Bonjour,

Vous avez récemment posté [un message]({server}/t/{topic_id}/{post_id}) ne respectant pas certains critères aidant à entretenir une bonne communication sur ce forum&nbsp;:

{items}

{order}

Si vous pensez que cet avertissement est une erreur, veuillez nous en excuser. [Vous pouvez le signaler sur ce topic.](https://forum.monnaie-libre.fr/t/developpement-dun-plugin-discourse-pour-limiter-le-nombre-de-posts-par-jour-par-personne/7121) (c'est d'autant plus probable que ce robot de modération est en développement)

:robot: Ce message est envoyé par un robot qui ne saura pas lire vos réponses."""
MESSAGE_WARNING_TEXT = """:warning: L'équipe de modération vous prie de suivre ces règles, dans le cas contraire des mesures pourront être prises."""
MESSAGE_SANCTION_TEXT = """:man_judge: Plusieurs avertissements ayant été émis, des mesures seront prises."""

def target_new_post(users, topic, post,  **kwargs):
	user = users[post["user_id"]]
	if SETTINGS["moderate_staff"] and (user["admin"] or user["moderator"]):
		return {}
	
	text = utils.clean_html(post["cooked"])
	infringed = []
	
	if len(text) > SETTINGS["post_chars_count_max"]:
		infringed.append("too_long")
	
	print(text)
	
	if len(infringed) > 0:
		user.warnings += 1
		if user.warnings < SETTINGS["warnings"]:
			return {
				"actions": [
					{
						"action": "send_message",
						"kwargs": {
							"user": user,
							"text": MESSAGE_TEXT.format(
								server=kwargs["server"],
								topic_id=topic.id,
								post_id=post["id"],
								items="\n".join([" * {}".format(ITEMS[i]) for i in infringed]),
								order=MESSAGE_WARNING_TEXT
							)
						}
					}
				]
			}
		else:
			return {
				"actions": [
					{
						"action": "send_message",
						"kwargs": {
							"user": user,
							"text": MESSAGE_TEXT.format(
								server=kwargs["server"],
								topic_id=topic.id,
								post_id=post["id"],
								items="\n".join([" * {}".format(ITEMS[i]) for i in infringed]),
								order=MESSAGE_SANCTION_TEXT
							)
						}
					}
				]
			}
	
	return {}
