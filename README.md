# Discourse moderator bot

## Installation

Create a user on the forum, and generate an API key for this user.

Install packages: `python3 python3-pip`

Install Python dependencies:

    sudo pip3 install --upgrade pydiscourse ubjson

Create `config.json` file:

    {
        "server": "https://discourse.example.net",
        "username": "robocop",
        "key": "ac3d7ed4b61b8ab79f1877c614bea4e18691e94170f6e22551edc0a209c033a7"
    }

Note: this example key is random; don't try it, it won't work.

Run it! The program will automatically add all the settings you did not set, to default values.

    python3 modbot.py

## Options

 * **--help**: Display help
 * **--oneshot**, **-O**: Do only one iteration
 * **--simulate**, **-S**: Do not act

## Configuration

Edit `config.json`:

 * **server**: Address of the Discourse instance
 * **username**: Name of the user on the Discourse instance
 * **key**: API key for the user on the Discourse instance
 * **modo_delay_max**: Maximum delay for _a posteriori_ moderation, in seconds. After this delay, a post won't be moderated. Default: 86400 (1 day)
