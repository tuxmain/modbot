#!/usr/bin/env python3

"""
	CopyLeft 2019 Pascal Engélibert

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import sys, os, shutil, time, json, importlib
import pydiscourse, ubjson

DATAFILE = "data.ubjson"
CONFIGFILE = "config.json"

SERVER = "https://forum.monnaie-libre.fr"
USERNAME = "modbot"
KEY = ""
MODO_DELAY_MAX = 86400
REFRESH_INTERVAL = 300
POSTS_CHUNK_SIZE = 20
RULES = {
	"std_clarity": {}
}
RULESLIB = {}

topics_seen = {}
users = {}

to_time = lambda t: time.mktime(time.strptime(t.split(".")[0], "%Y-%m-%dT%H:%M:%S"))

class User:
	def __init__(self, user_id, username, warnings=0, topic_warning=None):
		self.id = user_id
		self.username = username
		self.warnings = warnings
		self.topic_warning = topic_warning
	
	def export(self):
		return {
			"id": self.id,
			"username": self.username,
			"warnings": self.warnings,
			"topic_warning": self.topic_warning
		}

class Topic:
	def __init__(self, data, post_seen_latest=-1):
		self.data = data
		self.post_seen_latest = post_seen_latest
		
		self.id = data["id"]
		self.posts_count = data["posts_count"]
		self.last_post_date = to_time(data["last_posted_at"])
	
	def update(self, data):
		self.data = data
		
		self.id = data["id"]
		self.posts_count = data["posts_count"]
		self.last_post_date = to_time(data["last_posted_at"])
	
	def export(self):
		return {
			"data": self.data,
			"post_seen_latest": self.post_seen_latest
		}

def send_message(client, user, text, title="Modération automatique"):
	if user.topic_warning != None:
		try:
			data = client.topic(**user.topic_warning)
			if not True in [i["id"] == user.id for i in data["details"]["allowed_users"]]:
				user.topic_warning = None
		except pydiscourse.exceptions.DiscourseError:
			user.topic_warning = None
	
	if user.topic_warning == None:
		data = client.create_post(
			content = text,
			title = title,
			target_usernames = user.username,
			archetype = "private_message"
		)
		user.topic_warning = {"topic_id": data["topic_id"], "slug": data["topic_slug"]}
		print(data)
	else:
		print(client.create_post(
			content = text,
			topic_id = user.topic_warning["id"],
			target_usernames = user.username,
			archetype = "private_message"
		))
	print(user.topic_warning)

if __name__ == "__main__":
	if "--help" in sys.argv:
		print("""Modbot
CopyLeft 2019 Pascal Engélibert

Options:
  --help      Display help
  -O
  --oneshot   Iterate only one time
  -S
  --simulate  Do not act
""")
		exit()
	
	simulate = "-S" in sys.argv or "--simulate" in sys.argv
	if simulate:
		print("==== SIMULATION ====")
	
	print("Loading config...")
	configfile = open(CONFIGFILE, "r+")
	config = json.load(configfile)
	configfile.close()
	
	SERVER = config.setdefault("server", SERVER)
	USERNAME = config.setdefault("username", USERNAME)
	KEY = config.setdefault("key", KEY)
	MODO_DELAY_MAX = config.setdefault("modo_delay_max", MODO_DELAY_MAX)
	REFRESH_INTERVAL = config.setdefault("refresh_interval", REFRESH_INTERVAL)
	POSTS_CHUNK_SIZE = config.setdefault("posts_chunk_size", POSTS_CHUNK_SIZE)
	RULES = config.setdefault("rules", RULES)
	
	for rule in RULES:
		RULESLIB[rule] = importlib.import_module("rules.{}".format(rule))
		for setting in RULESLIB[rule].SETTINGS:
			RULESLIB[rule].SETTINGS[setting] = RULES[rule].setdefault(setting, RULESLIB[rule].SETTINGS[setting])
	
	configfile = open(CONFIGFILE, "w")
	json.dump(config, configfile, indent=1)
	configfile.close()
	
	print("Loading data...")
	if os.path.isfile(DATAFILE):
		datafile = open(DATAFILE, "rb")
		data = ubjson.load(datafile)
		topics_seen = {topic_seen["data"]["id"]: Topic(topic_seen["data"], topic_seen["post_seen_latest"]) for topic_seen in data["topics_seen"]}
		users = {user["id"]: User(user["id"], user["username"], user["warnings"], user["topic_warning"]) for user in data["users"]}
		datafile.close()
		shutil.copyfile(DATAFILE, DATAFILE+".bak")
	
	client = pydiscourse.DiscourseClient(SERVER, USERNAME, KEY)
	
	switch_actions = {
		"send_message": lambda kwargs: send_message(client, **kwargs)
	}
	
	try:
		while True:
			modified = False
			
			print("Getting latest topics...")
			topics_latest = client.latest_topics()
			
			for data in topics_latest["topic_list"]["topics"]:
				time.sleep(0.1)
				if not data["id"] in topics_seen \
				or topics_seen[data["id"]].last_post_date < to_time(data["last_posted_at"]) \
				or topics_seen[data["id"]].posts_count < data["posts_count"]:
					modified = True
					
					print("Getting topic {}".format(data["id"]))
					data = client.topic("", data["id"])
					
					if not data["id"] in topics_seen:
						topics_seen[data["id"]] = Topic(data)
					else:
						topics_seen[data["id"]].update(data)
					topic = topics_seen[data["id"]]
					
					print("Getting unseen posts in topic {}".format(topic.id))
					topic_posts = []
					chunk_ids = []
					for post_id in topic.data["post_stream"]["stream"]:
						if post_id > topic.post_seen_latest:
							chunk_ids.append(post_id)
							
							if len(chunk_ids) > POSTS_CHUNK_SIZE:
								topic_posts += client.posts(topic.id, chunk_ids)["post_stream"]["posts"]
								chunk_ids = []
					if len(chunk_ids) > 0:
						topic_posts += client.posts(topic.id, chunk_ids)["post_stream"]["posts"]
					
					for post in topic_posts:
						
						if not post["user_id"] in users:
							print("User: {}".format(post["user_id"]))
							
							users[post["user_id"]] = User(post["user_id"], post["username"])
						
						if time.time() - to_time(post["created_at"]) < MODO_DELAY_MAX: # check a posteriori delay
							print("Moderate: {}".format(post["id"]))
							for rulelib in RULESLIB:
								rule = RULESLIB[rulelib]
								if "target_new_post" in dir(rule):
									result = rule.target_new_post(
										users,
										topic,
										post,
										simulate = simulate,
										server = SERVER
									)
									print(result)
									if not simulate and "actions" in result:
										for action in result["actions"]:
											if action["action"] in switch_actions:
												switch_actions[action["action"]](action["kwargs"])
						
						topic.post_seen_latest = post["id"]
			
			if "--oneshot" in sys.argv or "-O" in sys.argv:
				break
			
			if modified:
				print("Saving data...")
				datafile = open(DATAFILE, "wb")
				ubjson.dump({
					"topics_seen": [topics_seen[topic_id].export() for topic_id in topics_seen],
					"users": [users[user].export() for user in users]
				}, datafile)
				datafile.close()
			
			time.sleep(REFRESH_INTERVAL)
			
	except KeyboardInterrupt:
		pass
	finally:
		print("Saving data...")
		datafile = open(DATAFILE, "wb")
		ubjson.dump({
			"topics_seen": [topics_seen[topic_id].export() for topic_id in topics_seen],
			"users": [users[user].export() for user in users]
		}, datafile)
		datafile.close()
