#!/usr/bin/env python3

"""
	CopyLeft 2019 Pascal Engélibert

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import re, html

word_separators = [" ", ",", ".", ";", ":", "?", "!", "(", ")", "/", "+", "<", ">", "\t", "\n", "\r", "=", "…", "\u202f", '"', "\\"]
ngram_word_separator = " "

clean_html_r = re.compile("<.*?>")

def clean_html(text, remove_asides=True):
	if remove_asides:
		text = re.sub(re.compile("<aside(.+?)</aside>"), "", text)
	
	return html.unescape(re.sub(clean_html_r, '', text))

def find_words(text):
	words = []
	word = ""
	
	for c in text:
		if c in word_separators:
			if len(word) != 0:
				words.append(word)
				word = ""
		else:
			word += c
	if len(word) != 0:
		words.append(word)
	
	return words

def count_ngrams(words, nmax=5):
	ngrams = {}
	ngram =  [[] for n in range(nmax)]
	
	for w in words:
		for n in range(nmax):
			ngram[n].append(w)
			if len(ngram[n]) > n+1:
				ngram_str = ngram_word_separator.join(ngram[n][:n+1])
				if ngram_str in ngrams:
					ngrams[ngram_str] += 1
				else:
					ngrams[ngram_str] = 1
				ngram[n].pop(0)
	for n in range(nmax):
		if len(ngram[n]) > n+1:
			ngram_str = ngram_word_separator.join(ngram[n][:n])
			if ngram_str in ngrams:
				ngrams[ngram_str] += 1
			else:
				ngrams[ngram_str] = 1
	
	return ngrams

def apply_threshold(ngrams, omin=2):
	new_ngrams = {}
	
	for ngram in ngrams:
		if ngrams[ngram] >= omin:
			new_ngrams[ngram] = ngrams[ngram]
	
	return new_ngrams
